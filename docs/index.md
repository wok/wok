# Wok

A multirepo management tool.

---

Manage multiple git repos as part of a single workspace.

Wok utilizes git submodules and works as a package manager on top of them.

[Get started](getting-started.md){ .md-button .md-button--primary }
