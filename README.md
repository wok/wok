# Wok

[![status-badge](https://ci.codeberg.org/api/badges/12553/status.svg)](https://ci.codeberg.org/repos/12553)

**Wok** helps to organize multiple git repositories into a single multi-project workspace.

## Community

Meet us in the chat room: [#wok:matrix.org](https://matrix.to/#/#wok:matrix.org)
